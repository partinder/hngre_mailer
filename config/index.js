var util = require("util")
var env = process.env.NODE_ENV
if(!env) env = "development"
var config = util.format("./%s.config.js",env)

module.exports = require(config)