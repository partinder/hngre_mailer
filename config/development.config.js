module.exports= {
	db_host : "localhost",
	google_api: "AIzaSyAS70p3_-ZjeMR2rWqWApxM26fjZgr10D0",
	date_from: "2016-01-15",
	api_host: "https://api.hngre.com",
	api_token: "UBOhX3",
	notification_host : "http://52.74.200.187:3031",
	notification_token: "1234",
	inactiveDays : 7,
	notifications:{
		// *    *    *    *    *    * (seconds(optional), minutes, hours,day of month, month, day of week)
		"weekly-recommendation":{
			active : true,
			to: ["push","email","sms"],
			rule: "06 12  * * 3" // 12.05 every Wednesday
		},
		"no-group":{
			active: true,
			to: ["push","email","sms"],
			rule: "07 12 * * 5" // 12.05 every Friday
		},
		"no-group-use":{
			active: true,
			to: ["push","email","sms"],
			rule: "05 12 * * 4" // 12.05 every Thrusday
		},
		"inactive":{
			active: true,
			to: ["push","email","sms"],
			rule: "5 12 * * 1" // 12.05 every Monday
		}
	},
	smsTemplates : [
	"Discover the best food in Delhi with the hottest food app of the year, Hngre. Download the app now - http://hng.re/opme/jr3ickJtSt",
	"All of Delhi's favourite dishes at your finger tips. Download the Hngre App now to explore them. http://hng.re/opme/NSsdAGLtSt",
	"Hey foodie. Have you heard of the coolest food discovery app in Delhi, Hngre? Install the iOS/Android app now - http://hng.re/opme/nndtK7NtSt"]
	
}