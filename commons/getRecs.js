var helpers = require("./helpers")
var config = require("../config")
var mongoClient = require("mongodb").MongoClient
var request = require("request")

module.exports = function(users, callback) {

    helpers.asyncLoop(users.length, function(loop) {
            //console.log(loop.iteration())
            var user = users[loop.iteration()]
                //console.log(user._id, user.first_name)
            if (user.first_name == null) {
                console.log(user)
            }
            //console.log(user)
            console.log("Starting getting recc  for user : %s", user.first_name)
                //console.log(user)

            var reqObj = {
                "rejectUnauthorized": false,
                "url": "",
                "method":"GET",
                "headers" : {
                    'User-Agent': 'HngreConsumer/1.0.0 (Nexus/5; Android/2.3.3; Java/7.*.*)',
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }

            var reqHost = config.api_host

            if (user.location.lat) {
                reqObj.url = reqHost + "/merchants/recommend/" + user._id + "?lon=" + user.location.lon + "&lat=" + user.location.lat + "&token=" + config.api_token
            }
            if (user.location.neighbourhood) {
                reqObj.url = reqHost + "/merchants/recommend/" + user._id + "?country=" + user.location.country + "&region=" + user.location.region + "&locality=" + user.location.locality + "&neighbourhood=" + user.location.neighbourhood + "&token=" + config.api_token
            }
            if (!user.location.neighbourhood && !user.location.lat) {
                reqObj.url = reqHost + "/merchants/recommend/" + user._id + "?country=" + user.location.country + "&region=" + user.location.region + "&locality=" + user.location.locality + "&token=" + config.api_token
            }
            //console.log(reqHost)


            request(reqObj, function(err, response, body) {
                if (err) {
                    console.log("SOME Error from API")
                    console.log(response)
                    callback(err)

                } else {
                    if (response.statusCode !== 200) {
                        //console.log(response.statusCode)
                        console.log("SOME Error from API")
                        callback(response.statusCode)

                    } else {
                        //console.log(response.statusCode)
                        //console.log(JSON.parse(body))
                        body = JSON.parse(body)
                            //console.log(body.merchants[0])

                        if (body.merchants[0].dishes[0].scores) {
                            function compare(a, b) {
                                //console.log(a.dishes.length, b.dishes.length)
                                if (a.dishes[0].scores[user._id] < b.dishes[0].scores[user._id])
                                    return 1;
                                if (a.dishes[0].scores[user._id] > b.dishes[0].scores[user._id])
                                    return -1;
                                return 0;
                            }

                            body.merchants.sort(compare);

                        }

                        var dishes = []
                            // body.merchants.forEach(function(mer){
                            //     console.log(mer.dishes[0].scores[user._id])
                            // })

                        console.log("Got all recommendations")
                        console.log("Getting Branch LInks for recommended dishes")

                        helpers.asyncLoop(3, function(loopBr) {
                                var ran = loopBr.iteration()
                                    // console.log(body.merchants[ran].dishes[0])

                                // Get Branch Link
                                var branchObj = {
                                    sdk: "api",
                                    feature: "share",
                                    channel: "email",
                                    branch_key: "key_live_llaHDxYLT3Zwwewt353WokdcByci9bC2",
                                    data: {
                                        hngre_path: "dish",
                                        hngre_dishId: body.merchants[ran].dishes[0]._id,
                                        hngre_merchantId: body.merchants[ran]._id,
                                        hngre_title: body.merchants[ran].dishes[0].name

                                    }
                                }

                                var options = {
                                    uri: 'https://api.branch.io/v1/url',
                                    headers: {
                                        'Content-Type': 'application/json'
                                    },
                                    body: branchObj,
                                    method: "post",
                                    json: true

                                };

                                request(options, function(err, response, body1) {
                                    if (err) console.log(err)
                                    else {
                                        if (body.merchants[ran].address.neighbourhood.indexOf("Manhattan") != -1) {
                                            body.merchants[ran].address.neighbourhood.splice(body.merchants[ran].address.neighbourhood.indexOf('Manhattan'), 1)
                                        }



                                        var dish = {
                                            merchantName: body.merchants[ran].name,
                                            dishName: body.merchants[ran].dishes[0].name,
                                            dishPhoto: body.merchants[ran].dishes[0].photo,
                                            neighbourhood: body.merchants[ran].address.neighbourhood,
                                            branchLink: body1.url,
                                            _id: body.merchants[ran].dishes[0]._id,
                                            merchantId: body.merchants[ran]._id
                                        }
                                        if (body.merchants[0].dishes[0].scores) {
                                            dish.score = body.merchants[ran].dishes[0].scores[user._id]
                                        }
                                        dishes.push(dish)

                                    }
                                    loopBr.next()
                                })

                            },
                            function() {
                                console.log("All Dishes Branch Link added for %s", users[loop.iteration()].email)
                                users[loop.iteration()].dishes = dishes
                                loop.next()
                            })

                    }

                }

            })


        },
        function() {
            console.log("All Reccs with Dishes and Branch Link Fetchech")
            callback(null, users)
        })
}