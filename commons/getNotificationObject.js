var helpers = require('./helpers');
var _ = require("underscore")

module.exports = function(type, to, users) {
    var that = this

    switch (type) {
        case "weekly-recommendation":

            var notificationObjects = []
            users.forEach(function(user) {
                //console.log(user)

                user.subjectLocation = helpers.subjectLocation(user)
                //console.log(user.subjectLocation)
                user.veg_scale = helpers.getVegScale(user.veg_scale)

                var notificationObject = {}

                if (to.indexOf("push") != -1) {
                    
                    if (user.devices.length > 0) {
                        // Create push object
                        var ids = []
                        user.devices.forEach(function(device) {
                            ids.push(device.gcm_notification.token)
                        })
                        notificationObject["push"] = {
                            deviceIds: ids,
                            title: "Hngre",
                            body: "Have you tried " + user.dishes[0].dishName + " at " + user.dishes[0].merchantName +"? We think you’ll like it.",
                            data: {
                                hngre_api_uri: encodeURI("hngre://dish?dishId=" + user.dishes[0]._id + "&merchantId=" + user.dishes[0].merchantId + "&title=" + "Lunch")
                            }
                        }
                    }
                }

                if (to.indexOf("inapp") != -1) {
                   
                    notificationObject["inapp"] = {
                        data: {
                            title: "Whats for lunch today?",
                            body: "Have you tried " + user.dishes[0].dishName + " at " + user.dishes[0].merchantName +"? We think you'll like it.",
                            path: encodeURI("hngre://dish?dishId=" + user.dishes[0]._id + "&merchantId=" + user.dishes[0].merchantId + "&title=" + "Lunch"),
                            icon: user.dishes[0].dishPhoto,
                            to: "inapp",
                            event: type
                        },
                        userId: user._id
                    }
                }

                if (to.indexOf("sms") != -1) {
                  
                    if (!notificationObject.push && user.phone_verified) {
                        
                        notificationObject["sms"] = {
                            phone: user.phone,
                            body: "Have you tried " + user.dishes[0].dishName + " at " + user.dishes[0].merchantName +"? We think you'll like it. Check it out here - " + user.dishes[0].branchLink
                        }
                    }
                }

                if (to.indexOf("email") != -1) {
                   
                    if (user.dishes) {
                        //delete user._id
                        delete user.created
                        delete user.devices
                        delete user.location
                        delete user.phone_verified
                        delete user.phone

                        user.dishes.forEach(function(dish) {
                            delete dish._id
                            delete dish.merchantId
                        })
                        user.dishes.splice(-1, 1)
                        notificationObject.email = user
                    }
                }
                notificationObject["event"] = type
                notificationObject["userId"] = user._id

                notificationObjects.push(notificationObject)

            })
            return notificationObjects
            break;

        

        case "no-group":
            var notificationObjects = []
            users.forEach(function(user) {
                var notificationObject = {}
                if (to.indexOf("push") != -1) {
                    
                    if (user.devices.length > 0) {
                        // Create push object
                        var ids = []
                        user.devices.forEach(function(device) {
                            ids.push(device.gcm_notification.token)
                        })
                        notificationObject["push"] = {
                            deviceIds: ids,
                            title: "Hngre",
                            body: "Eating out with friends? Create a Group on Hngre to find the perfect restaurant.",
                            data: {
                                hngre_api_uri: encodeURI("hngre://group")
                            }
                        }
                    }
                }
                if (to.indexOf("email") != -1) {                   

                    notificationObject["email"] = {
                        "email": user.email,
                        "name": user.first_name
                    }
                }

                if (to.indexOf("sms") != -1) {
                    if (!notificationObject.push && user.phone_verified) {
                        notificationObject["sms"] = {
                            phone: user.phone,
                            body: "Eating out with friends? Create a Group on Hngre to find the perfect restaurant - http://hng.re/opme/Tzj6czZ1hq" 
                        }
                    }
                }

                notificationObject["event"] = type
                    // notificationObject["userId"] = user._id
                notificationObjects.push(notificationObject)
            })
            return notificationObjects
            break;

        

        case "no-group-use":
            var notificationObjects = []
            users.forEach(function(user) {
                var notificationObject = {}
                if (to.indexOf("push") != -1) {
                    
                    if (user.devices.length > 0) {
                        // Create push object
                        var ids = []
                        user.devices.forEach(function(device) {
                            ids.push(device.gcm_notification.token)
                        })
                        notificationObject["push"] = {
                            deviceIds: ids,
                            title: "Hngre",
                            body: "Tried using Groups on Hngre? Never has it been easier to plan a group meal. Go ahead. Create a Group. U’ll thank us later.",
                            data: {
                                hngre_api_uri: encodeURI("hngre://group")
                            }
                        }
                    }
                }

                if (to.indexOf("email") != -1) {                   

                    notificationObject["email"] = {
                        "email": user.email,
                        "name": user.first_name
                    }
                }

                if (to.indexOf("sms") != -1) {
                    if (!notificationObject.push && user.phone_verified) {
                        notificationObject["sms"] = {
                            phone: user.phone,
                            body: "Tried using Groups on Hngre? It's the best tool for planning a group meal.\n\nGo ahead, u'll thank us later: http://hng.re/opme/Tzj6czZ1hq"

                        }
                    }
                }

                notificationObject["event"] = type
                    // notificationObject["userId"] = user._id
                notificationObjects.push(notificationObject)
            })            
            return notificationObjects
            break;

        

        case "inactive":

            var notificationObjects = []
            users.forEach(function(user) {
                //console.log(user)

                user.subjectLocation = helpers.subjectLocation(user)
                //console.log(user.subjectLocation)
                user.veg_scale = helpers.getVegScale(user.veg_scale)

                var notificationObject = {}

                if (to.indexOf("push") != -1) {
                    
                    if (user.devices.length > 0) {
                        // Create push object
                        var ids = []
                        user.devices.forEach(function(device) {
                            ids.push(device.gcm_notification.token)
                        })
                        notificationObject["push"] = {
                            deviceIds: ids,
                            title: "Hngre",
                            body: "Time for lunch? See what dishes we’ve picked out for you.",
                            data: {
                                hngre_api_uri: encodeURI("hngre://recommend")
                            }
                        }
                    }
                }

                // if (to.indexOf("inapp") != -1) {
                   
                //     notificationObject["inapp"] = {
                //         data: {
                //             title: "Whats for lunch today?",
                //             body: "Why don't you try " + user.dishes[0].dishName + " in " + user.subjectLocation,
                //             path: encodeURI("hngre://dish?dishId=" + user.dishes[0]._id + "&merchantId=" + user.dishes[0].merchantId + "&title=" + "Lunch"),
                //             icon: user.dishes[0].dishPhoto,
                //             to: "inapp",
                //             event: type
                //         },
                //         userId: user._id
                //     }
                // }

                if (to.indexOf("sms") != -1) {
                  
                    if (!notificationObject.push && user.phone_verified) {
                        
                        notificationObject["sms"] = {
                            phone: user.phone,
                            body: "Have you decided what's for lunch today? See what dishes Hngre's picked out for you - http://hng.re/opme/eKW4TQEgiq "
                        }
                    }
                }

                if (to.indexOf("email") != -1) {
                   
                    if (user.dishes) {
                        //delete user._id
                        delete user.created
                        delete user.devices
                        delete user.location
                        delete user.phone_verified
                        delete user.phone

                        user.dishes.forEach(function(dish) {
                            delete dish._id
                            delete dish.merchantId
                        })
                        user.dishes.splice(-1, 1)
                        notificationObject.email = user
                    }
                }
                notificationObject["event"] = type
                notificationObject["userId"] = user._id

                notificationObjects.push(notificationObject)

            })
            return notificationObjects
            break;

    }
}