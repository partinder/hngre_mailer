var helpers = require("./helpers")
var config = require("../config")
var mongoClient = require("mongodb").MongoClient
var moment = require("moment")
var ObjectId = require("mongodb").ObjectId
var _ = require("underscore")
module.exports = function(type, callback) {

    mongoClient.connect("mongodb://" + config.db_host + "/db_hngre", function(err, db) {
        if (err) {
            config.log(err)
            callback(err)
        } else {
            switch (type) {
                case "weekly-recommendation":
                    console.log("Finding users registered after : %s", config.date_from)
                    var date = config.date_from + "T00:00:00.000Z"
                    var count = 0
                    db.collection("users").find({
                        "created": {
                            $gte: new Date(date)
                        },
                        "email": {
                            $ne: null
                        }
                    }, {
                        "first_name": 1,
                        "last_name": 1,
                        "email": 1,
                        "veg_scale": 1,
                        "created": 1,
                        "devices": 1,
                        "phone": 1,
                        "phone_verified": 1
                    }).toArray(function(err, res) {
                        //console.log(res.length)
                        var users = []

                        if (err) callback(err)
                        else {
                            helpers.asyncLoop(res.length, function(loop) {

                                db.collection("recommendations").find({
                                    "_user": res[loop.iteration()]._id,
                                    "params": {
                                        $exists: true
                                    }
                                }).sort({
                                    "time_sent": -1
                                }).toArray(function(err, reccs) {
                                    if (reccs.length > 0) {
                                        count++
                                        if (!reccs[0].params.neighbourhood && !reccs[0].params.lat && !reccs[0].params.locality) {
                                            console.log("NOT FOUND")
                                            console.log(reccs[0].params)
                                                //count ++
                                        } else {
                                            if (reccs[0].params.lat) {
                                                res[loop.iteration()].location = {
                                                    lat: reccs[0].params.lat,
                                                    lon: reccs[0].params.lon
                                                }
                                            }
                                            if (reccs[0].params.neighbourhood) {
                                                res[loop.iteration()].location = {
                                                    neighbourhood: reccs[0].params.neighbourhood,
                                                    locality: reccs[0].params.locality,
                                                    region: reccs[0].params.region,
                                                    country: reccs[0].params.country

                                                }
                                            }
                                            if (!reccs[0].params.neighbourhood && !reccs[0].params.lat) {
                                                res[loop.iteration()].location = {
                                                    locality: reccs[0].params.locality,
                                                    region: reccs[0].params.region,
                                                    country: reccs[0].params.country

                                                }
                                            }
                                            if (res[loop.iteration()].first_name == null) {
                                                res[loop.iteration()].first_name = ""
                                                res[loop.iteration()].last_name = ""
                                            }

                                            users.push(res[loop.iteration()])

                                        }

                                    }
                                    loop.next()

                                })
                            }, function() {
                                console.log("Users registered after, %s are: %s", date, users.length)
                                    // users.forEach(function(user) {
                                    //     console.log(user)
                                    // })
                                callback(null, users)
                            })


                        }
                    })


                    // End Get Users 
                    break;



                case "no-group":
                    console.log("Finding users registered after : %s and still have'nt created a group", config.date_from)
                    var date = config.date_from + "T00:00:00.000Z"
                    var count = 0

                    db.collection("users").find({
                        phone_verified: false,
                        "created": {
                            $gte: new Date(date)
                        }
                    }, {
                        "first_name": 1,
                        "last_name": 1,
                        "email": 1,
                        "veg_scale": 1,
                        "created": 1,
                        "devices": 1,
                        "phone": 1,
                        "phone_verified": 1
                    }).toArray(function(err, res) {
                        if (err) {
                            console.log(err)
                        } else {
                            console.log("Calling Back")
                            callback(null, res)
                        }
                    })
                    break;



                case "no-group-use":
                    console.log("Finding users registered after : %s who have created groups but not using the group Reccs", config.date_from)
                    var date = moment().subtract(config.inactiveDays, "days").startOf('day')
                    var count = 0
                    var date_from = config.date_from + "T00:00:00.000Z"
                    var reccIds = []
                    var userIds = []

                    db.collection("recommendations").find({
                        "rec_merchants": {
                            $exists: true
                        }

                    }).toArray(function(err, res) {
                        if (err)
                            console.log(err)
                        else {
                            console.log("Total group reccs are: ", res.length)


                            res.forEach(function(reccs1) {
                                reccs1.rec_merchants.forEach(function(recc) {
                                    recc.forEach(function(recco) {
                                        if (recco._dishes) {
                                            if (recco._dishes.length > 1) {
                                                if (reccIds.indexOf(reccs1._id.toString()) == -1) {
                                                    reccIds.push(reccs1._id.toString())

                                                }

                                            }
                                        }
                                    })

                                })
                            })
                        }



                        for (i in reccIds) {
                            reccIds[i] = ObjectId(reccIds[i])
                        }


                        db.collection("recommendations").find({
                            "_id": {
                                $in: reccIds
                            },
                            "time_sent": {
                                $lte: new Date(date)
                            }
                        }).toArray(function(err, reccs2) {
                            if (err)
                                console.log(err)
                            else {

                                reccs2.forEach(function(recc) {
                                    if (userIds.indexOf(recc._user.toString()) == -1) {
                                        userIds.push(recc._user.toString())
                                    }
                                })
                            }

                            for (i in userIds) {
                                userIds[i] = ObjectId(userIds[i])
                            }

                            db.collection("users").find({
                                "phone_verified": true,
                                "_id": {
                                    $in: userIds
                                },
                                "created": {
                                    $gte: new Date(date_from)
                                }
                            }, {
                                "first_name": 1,
                                "last_name": 1,
                                "email": 1,
                                "veg_scale": 1,
                                "created": 1,
                                "devices": 1,
                                "phone": 1,
                                "phone_verified": 1
                            }).toArray(function(err, users) {
                                if (err)
                                    console.log(err)
                                else {
                                    console.log("Found such Users, Count : %s", users.length)
                                    callback(null, users)
                                }
                            })
                        })
                    })
                    break;



                case "inactive":

                    var date = moment().subtract(config.inactiveDays, "days").startOf('day')
                    var date_from = config.date_from + "T00:00:00.000Z"
                        //console.log(date.toISOString())
                        //console.log(date.toISOString())
                    var ids = []
                    console.log(new Date(date))


                    db.collection("recommendations").find({
                        "time_sent": {
                            $lte: new Date(date)
                        },
                        "rec_merchants": {
                            $exists: true
                        }

                    }).toArray(function(err, res) {

                        res.forEach(function(res1) {
                            if (ids.indexOf(res1._user.toString()) == -1) {
                                ids.push(res1._user.toString())
                                    //console.log(res1.time_sent,res1._id,res1._user)
                            }
                        })

                        for (i in ids) {
                            ids[i] = ObjectId(ids[i])
                        }

                        db.collection("users").find({
                            _id: {
                                $in: ids
                            },
                            "created": {
                                $gte: new Date(date_from)
                            }
                        }, {
                            "first_name": 1,
                            "last_name": 1,
                            "email": 1,
                            "veg_scale": 1,
                            "created": 1,
                            "devices": 1,
                            "phone": 1,
                            "phone_verified": 1

                        }).toArray(function(err, res) {
                            if (err) {
                                callback(err)
                            } else {

                                var users = []

                                if (err) callback(err)
                                else {
                                    //console.log(res)
                                    helpers.asyncLoop(res.length, function(loop) {

                                        db.collection("recommendations").find({
                                            "_user": res[loop.iteration()]._id,
                                            "params": {
                                                $exists: true
                                            }
                                        }).sort({
                                            "time_sent": -1
                                        }).toArray(function(err, reccs) {
                                            if (reccs.length > 0) {
                                                count++
                                                if (!reccs[0].params.neighbourhood && !reccs[0].params.lat && !reccs[0].params.locality) {
                                                    // console.log("NOT FOUND")
                                                    // console.log(reccs[0].params)
                                                    //count ++
                                                } else {
                                                    if (reccs[0].params.lat) {
                                                        res[loop.iteration()].location = {
                                                            lat: reccs[0].params.lat,
                                                            lon: reccs[0].params.lon
                                                        }
                                                    }
                                                    if (reccs[0].params.neighbourhood) {
                                                        res[loop.iteration()].location = {
                                                            neighbourhood: reccs[0].params.neighbourhood,
                                                            locality: reccs[0].params.locality,
                                                            region: reccs[0].params.region,
                                                            country: reccs[0].params.country

                                                        }
                                                    }
                                                    if (!reccs[0].params.neighbourhood && !reccs[0].params.lat) {
                                                        res[loop.iteration()].location = {
                                                            locality: reccs[0].params.locality,
                                                            region: reccs[0].params.region,
                                                            country: reccs[0].params.country

                                                        }
                                                    }
                                                    if (res[loop.iteration()].first_name == null) {
                                                        res[loop.iteration()].first_name = ""
                                                        res[loop.iteration()].last_name = ""
                                                    }
                                                    users.push(res[loop.iteration()])
                                                }
                                            }
                                            loop.next()

                                        })
                                    }, function() {
                                        console.log("Users inactive for the last %s days are : %s", config.inactiveDays, users.length)
                                            // users.forEach(function(user) {
                                            //     console.log(user)
                                            // })
                                        callback(null, users)
                                    })


                                }
                            }
                            //console.log(users)
                        })

                    })




                    break;

            
            }
        }
    })
}