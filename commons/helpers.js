var request = require("request")
var config = require("../config")
var fs = require("fs")
var mongoClient = require("mongodb").MongoClient
var async = require("async")
var moment = require("moment")
var ObjectId = require("mongodb").ObjectId



module.exports = {
    asyncLoop: function(iterations, func, callback) {
        var index = 0;
        var done = false;
        var loop = {
            next: function() {
                if (done) {
                    return;
                }

                if (index < iterations) {
                    index++;
                    func(loop);

                } else {
                    done = true;
                    callback();
                }
            },

            iteration: function() {
                return index - 1;
            },

            break: function() {
                done = true;
                callback();
            }
        };
        loop.next();
        return loop;
    },
    saveUsers: function(db, users, callback) {
        this.asyncLoop(users.length, function(loop) {

            var user = users[loop.iteration()]
                //delete user.location
            db.collection("notificationUsers").updateOne({
                "_id": user._id
            }, {
                $set: {
                    email: user.email,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    veg_scale: user.veg_scale,
                    location: user.location,
                    created: user.created,
                    devices: user.devices
                }
            }, {
                upsert: true
            }, function(err, count) {
                if (err) {
                    //console.log(err)
                    loop.next()
                } else {
                    //console.log(count.result)
                    loop.next()
                }
            })
        }, function() {
            callback()
        })
    },
    getUsers: function(db, query, callback) {
        db.collection("notificationUsers").find(query, {
            "notifications": 0
        }).toArray(function(err, users) {
            if (err) callback(err)
            else {
                callback(null, users)
            }
        })
    },
    updateNotifications: function(db, notification, body, callback) {
        db.collection("notifications").update({
            "_id": notification._id
        }, {
            $set: {
                "status": body
            }
        }, function(err, res) {
            if (err) callback(err)
            else {
                //console.log(res)
                callback()
            }
        })
    },
    getVegScale: function(scale) {
        if (scale.indexOf(1) !== -1)
            return "Vegan"
        if (scale.indexOf(2) !== -1)
            return "Vegetarian"
        return ""
    },
    saveNotifications: function(db, notifications, callback) {
        if (notifications.length == 0) {
            callback()
        } else {
            db.collection("notifications").insert(notifications, function(err, count) {
                if (err) {
                    //console.log(err)
                    callback(err)
                } else {
                    callback()
                }
            })
        }

    },
    getNotifications: function(db, query, callback) {
        db.collection("notifications").find(query).toArray(function(err, notifications) {
            if (err)
                callback(err)
            else {
                callback(null, notifications)
            }
        })
    },
    removeNotification: function(db, notification, callback) {
        //console.log(notification)
        db.collection("notifications").remove({
            "_id": notification._id
        }, function(err, count) {
            if (err)
                callback(err)
            else {
                callback()
            }
        })
    },
    subjectLocation: function(user) {

        //console.log("Getting Location")
        //console.log(user)
        if (user.location.lat) {
            return user.dishes[0].neighbourhood[0]
        }
        if (user.location.neighbourhood) {
            return user.location.neighbourhood
        }
        return user.dishes[0].neighbourhood[0]
    },
    getUsersTxt: function(db1, fileName, callback) {
        var that = this
        var count = 0
        console.log(process.env.NODE_ENV)

        if (process.env.NODE_ENV == "development") {
            var emails = fs.readFileSync("../data/" + fileName, "utf-8").toString().split("\n")
                //console.log("Hello")
        }
        if (process.env.NODE_ENV == "production") {
            var emails = fs.readFileSync("../data/" + fileName, "utf-8").toString().split("\n")
        }

        console.log(emails)
        mongoClient.connect("mongodb://" + config.db_host + "/db_hngre", function(err, db) {
            if (err) {
                config.log(err)
                callback(err)
            } else {
                db.collection("users").find({
                    "email": {
                        $in: emails
                    }
                }, {
                    "first_name": 1,
                    "last_name": 1,
                    "email": 1,
                    "veg_scale": 1,
                    "created": 1,
                    "devices": 1
                }).toArray(function(err, res) {

                    var users = []
                    if (err) callback(err)
                    else {

                        res.forEach(function(res1) {
                            if (emails.indexOf(res1.email) != -1) {
                                //console.log(emails[res1.email])
                                emails[emails.indexOf(res1.email)] = null
                            }
                        })

                        function isBigEnough(value) {
                            return value != null
                        }

                        var noUser = emails.filter(isBigEnough)
                            //console.log(noUser.length)
                        that.asyncLoop(res.length, function(loop) {

                            db.collection("recommendations").find({
                                "_user": res[loop.iteration()]._id,
                                "params": {
                                    $exists: true
                                }
                            }).sort({
                                "$natural": -1
                            }).toArray(function(err, reccs) {

                                if (reccs.length > 0) {
                                    count++

                                    if (reccs[0].params.lat) {
                                        res[loop.iteration()].location = {
                                            lat: reccs[0].params.lat,
                                            lon: reccs[0].params.lon
                                        }
                                    }
                                    if (reccs[0].params.neighbourhood) {
                                        res[loop.iteration()].location = {
                                            neighbourhood: reccs[0].params.neighbourhood,
                                            locality: reccs[0].params.locality,
                                            region: reccs[0].params.region,
                                            country: reccs[0].params.country

                                        }
                                    }
                                    if (!reccs[0].params.neighbourhood && !reccs[0].params.lat) {
                                        res[loop.iteration()].location = {
                                            locality: reccs[0].params.locality,
                                            region: reccs[0].params.region,
                                            country: reccs[0].params.country

                                        }
                                    }
                                    if (!reccs[0].params.neighbourhood && !reccs[0].params.lat && !reccs[0].params.locality) {
                                        console.log("NOT FOUND")
                                        console.log(reccs[0].params)
                                            //count ++
                                        loop.next()
                                    }

                                    //res[loop.iteration()].veg_scale = getVegScale(res[loop.iteration()].veg_scale)
                                    //console.log(res[loop.iteration()])



                                } else {
                                    //console.log(res[loop.iteration()])
                                    res[loop.iteration()].location = {
                                        locality: "New York",
                                        region: "New York",
                                        country: "United States"

                                    }

                                }
                                if (res[loop.iteration()].first_name == null) {
                                    res[loop.iteration()].first_name = ""
                                    res[loop.iteration()].last_name = ""
                                }

                                users.push(res[loop.iteration()])

                                loop.next()
                            })
                        }, function() {
                            console.log("Users from Txt file, %s", users.length)
                            callback(null, users, noUser)
                        })
                    }
                })

            }

        })
    },
    getPushUsers: function(users) {
        var pushUsers = []
        users.forEach(function(user) {
            var good = false
            if (user.devices) {
                for (i in user.devices) {
                    if (user.devices[i].active) {
                        if (user.devices[i].gcm_notification) {
                            if (user.devices[i].gcm_notification.active) {
                                good = true
                            } else {
                                user.devices[i] = null
                            }
                        } else {
                            user.devices[i] = null
                        }

                    } else {
                        //console.log("Else")
                        user.devices[i] = null
                    }
                }


                function isBigEnough(value) {
                    return value != null
                }

                user.devices = user.devices.filter(isBigEnough)
                    //pushUsers.push(user)

            }
        })
        return users
    }
}