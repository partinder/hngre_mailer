var http = require('http');
var cron = require("./notifications/cron")
var config = require("./config")
var mongoUrl = "mongodb://" + config.db_host + "/db_hngre_mailer"
var mongoClient = require("mongodb").MongoClient
var _ = require("underscore")

//Lets define a port we want to listen to
const PORT=3030; 

//We need a function which handles requests and send response
function handleRequest(request, response){
    response.end('It Works!! Path Hit: ' + request.url);
}

//Create a server
var server = http.createServer(handleRequest);

//Lets start our server
server.listen(PORT, function(){
    //Callback triggered when server is successfully listening. Hurray!
    //console.log(cron)
    console.log("Environment is ", process.env.NODE_ENV)
    console.log("Server listening on: http://localhost:%s", PORT);
});