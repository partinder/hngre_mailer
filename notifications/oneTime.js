var helpers = require('../commons/helpers');
var mongoClient = require("mongodb").MongoClient
var config = require("../config")
var getNotificationObject = require("../commons/getNotificationObject")
var findUsers = require("../commons/findUsers")
var getRecs = require("../commons/getRecs")
var mongoUrl = "mongodb://" + config.db_host + "/db_hngre"
var async = require("async")
var request = require("request")
var csv = require("csv")
var fs = require("fs")
var _ = require("underscore")

var client = require("twilio")("AC59b515897f70f729a9adf8d8d4f4938f", "24574be3b963db9b7cbf8024d59354d2")

var db
var dbNotifier



mongoClient.connect("mongodb://" + config.db_host + "/db_zomatoUsers,db_phoneNotifier", function(err, database) {
    // body...
    if (err)
        console.log(err)
    else {
        console.log("Connected to DB")
        db = database.db("db_zomatoUsers")
        dbNotifier = database.db("db_phoneNotifier")

        //phones8182(["sms"]) // Created on 2nd June
        dbNotifier.collection("june2nd").find({ "sent": { $exists: false } }).limit(5000).toArray(function(err, result) {
            if (err)
                console.log(err)
            else {
                console.log("fetched %s results", result.length)
            }
            sendTwilio(result)
        })

        // dbNotifier.collection("june2nd").find({ "sms.phone": "+919873756056" }).limit(5000).toArray(function(err, result) {
        //     if (err)
        //         console.log(err)
        //     else {
        //         console.log("fetched %s results", result.length)
        //     }
        //     sendTwilio(result)
        // })

    }
})




function phones8182(to) {

    var data1 = fs.readFileSync("../swipe/8-1.csv", "utf8")
    var data2 = fs.readFileSync("../swipe/8-2.csv", "utf8")

    csv.parse(data1, function(err, data1_csv) {
        //console.log(data[0][1])



        csv.parse(data2, function(err, data2_csv) {



            data2_csv.forEach(function(d) {
                data1_csv.push(d)
            })

            var emails = []
            var users = []
            data1_csv.forEach(function(user) {
                emails.push(user[1])
            })

            db.collection("finalSwipe").find({ "email": { $in: emails }, "phone": { $ne: "" }, "location": "Delhi NCR" }).toArray(function(err, res) {
                if (err)
                    console.log(err)
                else {

                    res.forEach(function(re) {
                        delete re._id


                        if (re.name.search(/zomato/i) == -1 && re.email.search(/zomato/i) == -1 && re.email.search(/zoman/i) == -1 && re.name.search(/zoman/i) == -1 && re.email.search(/urbanspoon/i) == -1 && re.name.search(/urbanspoon/i) == -1 && re.email.search(/foodiebay/i) == -1 && re.name.search(/foodiebay/i) == -1 && re.email.search(/spam/i) == -1 && re.name.search(/zoman/i) == -1) {
                            if (re.phone.length == 13 && re.phone.startsWith("+91")) {
                                re.phone = re.phone.toString()
                                users.push(re)
                            } else if (re.phone.length == 10) {
                                re.phone = "+91" + re.phone
                                re.phone = re.phone.toString()
                                users.push(re)
                            } else if (re.phone.length == 12 && re.phone.startsWith("91")) {
                                re.phone = "+" + re.phone
                                re.phone = re.phone.toString()
                                users.push(re)
                                    //console.log(re.phone)
                            } else if (re.phone.length == 11 && !re.phone.startsWith("01") && re.phone.startsWith("0")) {
                                // console.log(re.phone)
                                re.phone = "+91" + re.phone.substring(1, 11)
                                re.phone = re.phone.toString()
                                    //console.log(re.phone)
                                users.push(re)
                            }
                        }
                    })
                }

                var notificationObejcts = []
                users.forEach(function(user) {
                    notificationObejcts.push({
                        "event": "sms-invite",
                        "sms": {
                            phone: user.phone,
                            body: config.smsTemplates[Math.floor(Math.random() * ((config.smsTemplates.length - 1) + 1))]
                        }
                    })
                })

                notificationObejcts.unshift({
                    "event": "sms-invite",
                    "sms": {
                        phone: "+919999009196",
                        body: config.smsTemplates[Math.floor(Math.random() * ((config.smsTemplates.length - 1) + 1))]
                    }
                })

                // notificationObejcts.push({
                //     "event": "sms-invite",
                //     "sms": {
                //         phone: "+919999009196",
                //         body: config.smsTemplates[Math.floor(Math.random() * ((config.smsTemplates.length - 1) + 1))]
                //     }
                // })

                //send([notificationObejcts[notificationObejcts.length-1]])

                console.log("Total NOtigicatiaons to unders: %s", notificationObejcts.length)

                dbNotifier.collection("june2nd").insert(notificationObejcts[0], { w: 0 }, function(err, docs) {
                    if (err)
                        console.log(err)
                    else {
                        console.log(docs.result)
                    }
                })
            })
        })
    })

}

function send(notifications) {

    var errors = []
    var tempId

    helpers.asyncLoop(notifications.length, function(loop) {
        var notificationObject = notifications[loop.iteration()]
        tempId = notificationObject._id
        delete notificationObject._id
        var parallel = []

        if (notificationObject.inapp) {
            // Send to Api server 
            parallel.push(function(callback) {
                var reqObj = {
                    uri: config.api_host + "/notifications/generate?token=" + config.api_token,
                    json: true,
                    body: notificationObject.inapp,
                    method: "post",
                    "headers": {
                        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36',
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }
                delete notificationObject.inapp

                request(reqObj, function(err, res, body) {
                    if (err) {
                        errors.push(err)
                        callback(null)
                    } else {
                        console.log(res.statusCode)

                        if (res.statusCode == 200 || res.statusCode == 201) {
                            updateUser(tempId, body)
                            console.log(_.keys(body)[1][0])
                        } else {
                            errors.push(body)
                        }

                        callback(null, body)
                    }
                })

            })

        }
        if (notificationObject.push || notificationObject.sms || notificationObject.email) {



            parallel.push(function(callback) {
                var reqObj = {
                    uri: config.notification_host + "/api/events?access_token=" + config.notification_token,
                    json: true,
                    body: notificationObject,
                    method: "post"
                }
                request(reqObj, function(err, res, body) {
                    if (err) {
                        errors.push(err)
                        callback(null)
                    } else {
                        if (res.statusCode == 200 || res.statusCode == 201) {
                            updateUser(tempId, body)
                            console.log("Notification send to %s", notificationObject["sms"]["phone"])
                        } else {
                            errors.push(body)
                        }

                        callback(null, body)
                    }
                })
            })
        }
        //console.log(parallel[0])


        async.parallel(parallel, function(err, data) {
            if (err)
                console.log(err)

        })
        loop.next()
            //notificatonObject["event"] = type
    }, function() {

        console.log("All DONE with %s errors", errors.length)
        if (errors.length > 0) {
            errors.forEach(function(err) {
                console.log(err)
            })
        }

        //process.exit(0)


    })
}

function updateUser(id, status, body) {
    dbNotifier.collection("june2nd").update({ "_id": id }, { $set: { "sent": status, "body": body } }, function(err, count, res) {
        if (err)
            console.log(err)
        else {
            console.log(count.result)
        }
    })
}

function sendTwilio(notifications) {

    helpers.asyncLoop(notifications.length, function(loop) {

        var notification = notifications[loop.iteration()]

        client.sendMessage({

            to: notification.sms.phone, // Any number Twilio can deliver to
            from: '+16466473426 ', // A number you bought from Twilio and can use for outbound communication
            body: notification.sms.body // body of the SMS message

        }, function(err, responseData) { //this function is executed when a response is received from Twilio

            if (!err) { // "err" is an error received during the request, if any

                // "responseData" is a JavaScript object containing data received from Twilio.
                // A sample response from sending an SMS message is here (click "JSON" to see how the data appears in JavaScript):
                // http://www.twilio.com/docs/api/rest/sending-sms#example-1

                console.log("Status for SMS to : %s , ", notification.sms.phone, responseData.status)
                updateUser(notification._id, true, responseData.status)


            } else {
                console.log(err)
                updateUser(notification._id, false, err)
            }
            setTimeout(function(){
                loop.next()
            },200)

          
        });

    }, function() {
        console.log("All Done")
        process.exit(0)
    })

}