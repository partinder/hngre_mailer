var helpers = require("../commons/helpers")
var request = require("request")
var async = require("async")
var config = require("../config")
var _ = require("underscore")
var mongoUrl = "mongodb://" + config.db_host + "/db_hngre_mailer"
var mongoClient = require("mongodb").MongoClient

module.exports = function(cb) {

    mongoClient.connect(mongoUrl, function(err, db) {
        if (err)
            cb(err)
        else {
            helpers.getNotifications(db, {
                "status": "fresh"
            }, function(err, notifications) {
                if (err) {
                    console.log(err)
                    cb(err)
                } else {
                    var errors = []
                    var response_errors = []
                    console.log("Number of notitications to send : %s", notifications.length)
                    helpers.asyncLoop(notifications.length, function(loop) {

                        var notificationObject = notifications[loop.iteration()]
                        var tempid = notificationObject._id
                        var userId = notificationObject.userId

                        delete notificationObject._id
                        delete notificationObject.status
                        delete notificationObject.userId

                        console.log(notificationObject)
                        var parallel = []

                        if (notificationObject.inapp) {
                            // Send to Api server 
                            parallel.push(function(callback) {
                                var reqObj = {
                                    uri: config.api_host + "/notifications/generate?token=" + config.api_token,
                                    json: true,
                                    body: notificationObject.inapp,
                                    method: "post",
                                    "headers": {
                                        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36',
                                        'Content-Type': 'application/x-www-form-urlencoded'
                                    }
                                }
                                delete notificationObject.inapp
                                request(reqObj, function(err, res, body) {
                                    if (err) callback(err)
                                    else {
                                        notificationObject._id = tempid
                                        callback(null, body)
                                    }
                                })

                            })

                        }
                        if (notificationObject.push || notificationObject.sms || notificationObject.email) {
                            parallel.push(function(callback) {
                                var reqObj = {
                                    uri: config.notification_host + "/api/events?access_token=" + config.notification_token,
                                    json: true,
                                    body: notificationObject,
                                    method: "post"
                                }
                                request(reqObj, function(err, res, body) {
                                    if (err) callback(err)
                                    else {
                                        
                                        notificationObject._id = tempid
                                        callback(null, body)
                                    }
                                })
                            })
                        }
                        //console.log(parallel[0])


                        async.parallel(parallel, function(err, data) {
                                if (err) {
                                    //console.log(err)                            
                                    notificationObject._id = tempid
                                    notificationObject.userId = userId
                                    errors.push(err)
                                    notificationObject._id = tempid
                                    helpers.updateNotifications(db, notificationObject, err, function(err) {
                                            if (err) {
                                                console.log(err)
                                            }
                                        })
                                        //loop.next()
                                } else {

                                    notificationObject._id = tempid
                                    notificationObject.userId = userId
                                    var bodyNew = {}
                                    data.forEach(function(res) {
                                        _.extend(bodyNew, res)
                                    })
                                    helpers.updateNotifications(db, notificationObject, bodyNew, function(err) {
                                            if (err) {
                                                console.log(err)
                                            }
                                        })
                                        //loop.next()
                                }
                                loop.next()
                            })
                            //notificatonObject["event"] = type
                    }, function() {
                        if (errors.length == 0) {
                            errors = null
                        }
                        if (response_errors.length == 0) {
                            response_errors = null
                        }
                        //console.log(response_errors)
                        cb(errors, response_errors)


                    })
                }
            })
        }
    })

}