var helpers = require('../commons/helpers');
var mongoClient = require("mongodb").MongoClient
var config = require("../config")

module.exports = function(db, type, callback) {
    switch (type) {
        case "weekly-push-1":
            helpers.getUsersDate(db, config.date_from, function(err, users) {
                if (err)
                    callback(err)
                else {
                    helpers.saveUsers(db, users, function(err) {
                        if (err) callback(err)
                        else {
                            helpers.getUsers(db, {}, function(err, users) {
                                if (err) {
                                    callback(err)
                                } else {
                                    console.log("Collected Users to send Push to")
                                    console.log("Creating User Objects for Push Notifications")
                                    helpers.getReccsMerchant(users, function(err, users) {
                                        if (err) {
                                            callback(err)
                                        } else {
                                            console.log("Finalised Users with their Objects to send Email")
                                            //console.log(users)
                                            users.forEach(function(user) {
                                                delete user._id
                                                delete user.created
                                                delete user.devices
                                                user.subjectLocation = helpers.subjectLocation(user)
                                                user.veg_scale = helpers.getVegScale(user.veg_scale)
                                                //console.log(user)
                                            })
                                            helpers.saveEmails(db, users, function(err) {
                                                if (err) {
                                                    callback(err)
                                                } else {
                                                    //console.log("Email ready to Send")
                                                    //callback()
                                                }
                                            })
                                            callback(null, users)
                                        }
                                    })

                                }
                            })
                        }
                    })
                }
            })
            break;

        case "manual-push":
            helpers.getUsersTxt(db, "test.txt", function(err, users) {
            users = helpers.getPushUsers(users)
            console.log("Total Users to send Push Notofication to : %s", users.length)
            
                
            })
    }
}