var helpers = require('../commons/helpers');
var mongoClient = require("mongodb").MongoClient
var config = require("../config")
var getNotificationObject = require("../commons/getNotificationObject")
var findUsers = require("../commons/findUsers")
var getRecs = require("../commons/getRecs")
var mongoUrl = "mongodb://" + config.db_host + "/db_hngre_mailer"

module.exports = function(type, to, callback) {
    console.log("Setting  %s Up notifications",type)

    mongoClient.connect(mongoUrl, function(err, db) {
        if (err)
            callback(err)
        else {

            findUsers(type, function(err, users) {
                if (err)
                    callback(err)
                else {

                    switch (type) {

                        case "weekly-recommendation": // Send Reccomendation Notification registered after a particular date
                            console.log("Fetching email objects for Users registered after : %s", config.date_from)

                            //console.log(users)
                            getRecs(users, function(err, notifications) {
                                if (err) {
                                    callback(err)
                                } else {
                                    callback(finaliseObject(notifications))
                                }
                            })

                            break;

                        case "no-group":

                            callback(finaliseObject(users))
                            break;

                        case "no-group-use":
                            callback(finaliseObject(users))
                            break;

                        case "inactive":
                            console.log("Number of %s users are : %s",type,users.length)

                            getRecs(users, function(err, notifications) {
                                if (err) {
                                    callback(err)
                                } else {
                                    //console.log(notifications[0].dishes)
                                    callback(finaliseObject(notifications))

                                }
                            })
                            break;
                    }

                }
            })

        }

        function finaliseObject(notifications) {
            notifications = helpers.getPushUsers(notifications) // Find users with Push access
            notifications = getNotificationObject(type, to, notifications)
            notifications = notifications.map(function(am) {
                am["status"] = "fresh"
                return am
            })
            console.log("Finalised Notitication Objects, and saving them to DB")
            console.log(notifications)
            helpers.saveNotifications(db, notifications, function(err) {
                if (err) {
                    return err
                } else {
                    return null
                }
            })
        }
    })



}