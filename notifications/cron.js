var helpers = require('../commons/helpers');
var mongoClient = require("mongodb").MongoClient
var config = require("../config")
var setUpNotifications = require("./setUpNotifications")
var setupPush = require("./setUpPush")
var schedule = require("node-schedule");
var mongoUrl = "mongodb://" + config.db_host + "/db_hngre_mailer"
var _ = require("underscore")
var async = require("async")
var sendNotifications = require("./sendNotifications")
var ObjectId = require("mongodb").ObjectID
var request = require("request")
var CronJob = require("cron")
var CronJob = require('cron').CronJob;

var weeklyrecommendation
var nogroup
var nogroupuse
var inactive


var crons = []
var j




    //console.log(keys)
var keys = _.keys(config.notifications)

helpers.asyncLoop(keys.length, function(loop) {


    var notification = config.notifications[keys[loop.iteration()]]
    var setup_rule = notification.rule.split(" ")
    var cron_rule = notification.rule
    setup_rule[1] = (parseInt(setup_rule[1]) - 6).toString()
    setup_rule = setup_rule.join(" ")
        //console.log("HELLO HELLO")\

    if (notification.active) {
     

        var temp = keys[loop.iteration()]
        console.log(setup_rule,temp)

        var a = new CronJob(setup_rule, function() {            
                //console.log("Setting up %s",temp)
                setUpNotifications(temp, notification.to, function(err) {
                    if (err) {
                        console.log(err)
                    } else {
                        console.log("notifications Setup")
                    }
                })

            }, function() {
                /* This function is executed when the job stops */
            },
            true, /* Start the job right now */
            "Asia/Kolkata" /* Time zone of this job. */
        );
        crons.push(a)        

       var b = new CronJob(cron_rule, function() {
                sendNotifications(function(err, res) {
                    console.log(err, res)
                    if (err) {
                        console.log("Major Error", err)
                    } else {
                        if (res) {
                            console.log("Response Errors", res)
                        } else {
                            console.log("All Done no Errors")
                        }
                    }
                })
            }, function() {
                /* This function is executed when the job stops */
            },
            true, /* Start the job right now */
            "Asia/Kolkata" /* Time zone of this job. */
        );
       crons.push(b)
        //a()
        loop.next()

    }

}, function() {})

//console.log(crons)


module.exports = crons