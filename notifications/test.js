var helpers = require('../commons/helpers');
var mongoClient = require("mongodb").MongoClient
var config = require("../config")
var getNotificationObject = require("../commons/getNotificationObject")
var findUsers = require("../commons/findUsers")
var getRecs = require("../commons/getRecs")
var mongoUrl = "mongodb://" + config.db_host + "/db_hngre"
var async = require("async")
var request = require("request")

var emails = ["suresh.mahawar@hngre.com"]
var to = ["push"]

mongoClient.connect(mongoUrl, (err, db) => {
    if (err)
        console.log(err)
    else {
        console.log("Connected to Db")

        // Find Users

        db.collection("users").find({
            "email": { $in: emails }
        }, {
            "first_name": 1,
            "last_name": 1,
            "email": 1,
            "veg_scale": 1,
            "created": 1,
            "devices": 1,
            "phone": 1,
            "phone_verified": 1
        }).toArray(function(err, res) {
            if (err) {
                console.log(err)
            } else {


                console.log("Found %s users from list of %s", res.length, emails.length)
                    // Create Notification Object
                var notifications = []
                var users_recc = []

                helpers.asyncLoop(res.length, function(loop) {

                    db.collection("recommendations").find({
                        "_user": res[loop.iteration()]._id,
                        "params": {
                            $exists: true
                        }
                    }).sort({
                        "time_sent": -1
                    }).toArray(function(err, reccs) {
                        if (reccs.length > 0) {
                            //count++
                            if (!reccs[0].params.neighbourhood && !reccs[0].params.lat && !reccs[0].params.locality) {
                                console.log("NOT FOUND")
                                console.log(reccs[0].params)
                                    //count ++
                            } else {
                                if (reccs[0].params.lat) {
                                    res[loop.iteration()].location = {
                                        lat: reccs[0].params.lat,
                                        lon: reccs[0].params.lon
                                    }
                                }
                                if (reccs[0].params.neighbourhood) {
                                    res[loop.iteration()].location = {
                                        neighbourhood: reccs[0].params.neighbourhood,
                                        locality: reccs[0].params.locality,
                                        region: reccs[0].params.region,
                                        country: reccs[0].params.country

                                    }
                                }
                                if (!reccs[0].params.neighbourhood && !reccs[0].params.lat) {
                                    res[loop.iteration()].location = {
                                        locality: reccs[0].params.locality,
                                        region: reccs[0].params.region,
                                        country: reccs[0].params.country

                                    }
                                }
                                if (res[loop.iteration()].first_name == null) {
                                    res[loop.iteration()].first_name = ""
                                    res[loop.iteration()].last_name = ""
                                }

                                users_recc.push(res[loop.iteration()])

                            }

                        }
                        loop.next()

                    })
                }, function() {
                    console.log(users_recc)
                    getRecs(users_recc, function(err, users) {
                        if (err)
                            console.log(err)
                        else {



                            users = helpers.getPushUsers(users)

                            var notifications = getNotificationObject("weekly-recommendation",to,users)

                            

                            notifications = notifications.map(function(am) {
                                am["status"] = "fresh"
                                return am
                            })

                            //console.log(notifications)

                            send(notifications)



                            // users.forEach(function(user) {
                            //     var notificationObject = {}
                            //     if (to.indexOf("push") != -1) {

                            //         if (user.devices.length > 0) {
                            //             // Create push object
                            //             var ids = []
                            //             user.devices.forEach(function(device) {
                            //                 ids.push(device.gcm_notification.token)
                            //             })
                            //             notificationObject["push"] = {
                            //                 deviceIds: ids,
                            //                 title: "Hngre",
                            //                 body: "Hello There I am testing new Push called TEST",
                            //                 data: {
                            //                     hngre_api_uri: encodeURI("hngre://")
                            //                 }
                            //             }
                            //         }
                            //     }
                            //     if (to.indexOf("email") != -1) {

                            //         notificationObject["email"] = {
                            //             "email": user.email,
                            //             "name": user.first_name
                            //         }
                            //     }

                            //     if (to.indexOf("sms") != -1) {
                            //         if (!notificationObject.push && user.phone_verified) {
                            //             notificationObject["sms"] = {
                            //                 phone: user.phone,
                            //                 body: "Hello There I am testing new Push called TEST"
                            //             }
                            //         }
                            //     }

                            //     notificationObject["event"] = "no-group"
                            //         // notificationObject["userId"] = user._id
                            //     notifications.push(notificationObject)
                            // })

                            // notifications = notifications.map(function(am) {
                            //     am["status"] = "fresh"
                            //     return am
                            // })
                            // console.log("Finalised Notitication Objects, now start Sending")
                            //     //console.log(notifications)

                            // send(notifications)
                        }
                    })

                })














            }
        })

    }
})

function send(notifications) {

    var errors = []
    var response_errors = []
    helpers.asyncLoop(notifications.length, function(loop) {

        var notificationObject = notifications[loop.iteration()]
        var tempid = notificationObject._id
        var userId = notificationObject.userId

        delete notificationObject._id
        delete notificationObject.status
        delete notificationObject.userId

        console.log(notificationObject)
        var parallel = []

        if (notificationObject.inapp) {
            // Send to Api server 
            parallel.push(function(callback) {
                var reqObj = {
                    uri: config.api_host + "/notifications/generate?token=" + config.api_token,
                    json: true,
                    body: notificationObject.inapp,
                    method: "post",
                    "headers": {
                        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.110 Safari/537.36',
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                }
                delete notificationObject.inapp
                request(reqObj, function(err, res, body) {
                    if (err) callback(err)
                    else {
                        notificationObject._id = tempid
                        callback(null, body)
                    }
                })

            })

        }
        if (notificationObject.push || notificationObject.sms || notificationObject.email) {
            parallel.push(function(callback) {
                var reqObj = {
                    uri: config.notification_host + "/api/events?access_token=" + config.notification_token,
                    json: true,
                    body: notificationObject,
                    method: "post"
                }
                request(reqObj, function(err, res, body) {
                    if (err) {
                        callback(err)
                        console.log(err)
                    }
                    else {
                        console.log(body)
                        notificationObject._id = tempid
                        callback(null, body)
                    }
                })
            })
        }
        //console.log(parallel[0])


        async.parallel(parallel, function(err, data) {
                if (err) {
                    //console.log(err)                            
                    notificationObject._id = tempid
                    notificationObject.userId = userId
                    errors.push(err)
                    notificationObject._id = tempid
                        // helpers.updateNotifications(db, notificationObject, err, function(err) {
                        //         if (err) {
                        //             console.log(err)
                        //         }
                        //     })
                        //loop.next()
                } else {

                    console.log("Send Notitication type %s to %s", notificationObject["type"])
                        //loop.next()
                }
                loop.next()
            })
            //notificatonObject["event"] = type
    }, function() {
        if (errors.length == 0) {
            errors = null
        }
        if (response_errors.length == 0) {
            response_errors = null
        }
        //console.log(response_errors)
        // cb(errors, response_errors)
        console.log("All DONE")


    })
}